# README #

This project contains C++ programs that generate a 2D dungeon in a command line environment.  
### Instructions ###
1. Open Dungeon/Dungeon.sln using Visual Studio and build the solution
2. Execute Dungeon.exe that was generated from the build using command prompt  
### Usage ###
Executing Dungeon.exe without any arguments will print the usage:  
Dungeon -s [seed] -w [width] -t [height]  
### Notes ###
The seed argument allows you to generate the same dungeon every time your run the program.