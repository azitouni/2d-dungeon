#include "room.h"
#include "position.h"
#include <vector>
#include <iostream>
#include <tuple>

Room::Room()
{
	position = Position(0, 0);
	width = 0;
	height = 0;
}
Room::Room(Position position, int width, int height)
{
	this->position = position;
	this->width = width;
	this->height = height;
}

Position Room::getPosition()
{
	return position;
}
int Room::getWidth()
{
	return width;
}
int Room::getHeight()
{
	return height;
}
std::vector<Position> Room::getArea()
{
	return area;
}

void Room::setPosition(int pos_i, int pos_j)
{
	this->position.pos_i = pos_i;
	this->position.pos_j = pos_j;
}

void Room::setWdith(int width)
{
	this->width = width;
}

void Room::setHeight(int height)
{
	this->height = height;
}

void Room::setDoor(Position doorPosition)
{
	this->door = doorPosition;
}
Position Room::getDoor()
{
	return door;
}

void Room::fillArea(Position position)
{
	area.push_back(position);
}

bool operator == (Room & r1, Room & r2)
{
	if (r1.getPosition().pos_i == r2.getPosition().pos_i && r1.getPosition().pos_j == r2.getPosition().pos_j)
	{
		return true;
	}
	return false;
}
