#include <vector>
#include <tuple>
#include "position.h"

#ifndef ROOM_H
#define ROOM_H
class Room
{
private:
	Position position;
	int width;
	int height;
	std::vector<Position> area;
	Position door;
public:
	Room();
	Room(Position position, int width, int height);
	Position getPosition();
	int getWidth();
	int getHeight();
	std::vector<Position> getArea();
	void fillArea(Position);
	void setDoor(Position doorPosition);
	Position getDoor();
	void setPosition(int pos_i, int pos_j);
	void setWdith(int width);
	void setHeight(int height);

	friend bool operator == (Room & r1, Room & r2);

};
#endif