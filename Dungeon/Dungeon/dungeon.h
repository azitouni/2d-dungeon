#include "position.h"
#include "room.h"
#include <vector>
#include <iostream>
#include <string>
#include <stdio.h>
#include <random>

class Dungeon
{
private:
	static const char id_tile_solid = 0;
	static const char id_tile_floor = 1;
	static const char id_tile_door = 2;
	static const char id_tile_entrance = 3;
	static const char id_tile_corridor = 4;
	static const char id_tile_horizontal_wall = 5;
	static const char id_tile_vertical_wall = 6;

	static const char tile_solid = ' ';
	static const char tile_floor = '#';
	static const char tile_door = 'D';
	static const char tile_entrance = '*';
	static const char tile_corridor = '.';
	static const char tile_horizontal_wall = '-';
	static const char tile_vertical_wall = '|';

	static const int max_room_area = 16;
	static const int max_room_width = 4;
	static const int max_room_height = 4;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> room_width_dist;
	std::uniform_int_distribution<int> room_height_dist;
	std::uniform_int_distribution<int> position_i_dist;
	std::uniform_int_distribution<int> position_j_dist;

	int seed;
	int width;
	int height;
	int room_count;
	std::vector<Room>rooms;
	Room default_room;
	std::vector<Position>corridors;
	std::vector< std::vector<int> >dungeon;
	void addWalls();
	void lastAdjustments();

public:
	Dungeon(int width, int height, int seed);
	void printDungeon();
	Room createRoom(Position position);
	void addFirstRoom();
	bool roomWithinBoundary(Position room_pos, int room_width, int room_height);
	bool addRoom();
	Position randomRoomWall(Room room);
	Position randomPosition();
	bool roomsCollide(Room room);
	bool hasSpaceForRoom(Room room);
	int generateRandomNumber(int start_range, int end_range);
	void generateCorridors();
	Room getClosestRoom(Room room);
	void buildDungeon();
	void adjustDungeon();
	void addEntrance();
	void printLegend();
};