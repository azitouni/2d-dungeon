#include "dungeon.h"
#include <vector>
#include <iostream>
#include <string>
#include <stdio.h>
#include <random>

using std::string;

void printUsage()
{
	fprintf(stderr, "Usage: %s %s %s %s %s %s %s\n", "Dungeon", "-s", "<seed>", "-w", "<width>", "-t", "<height>");
}

int main(int argc, char* argv[])
{
	int seed = 0;
	int width = 0;
	int height = 0;

	if (argc != 7)
	{
		printUsage();
		exit(1);
	}
	else
	{
		seed = atoi(argv[2]);
		width = atoi(argv[4]);
		height = atoi(argv[6]);
	}

	Dungeon dungeon(width, height, seed);
	dungeon.buildDungeon();

	return 0;
}