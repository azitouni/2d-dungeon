#ifndef POSITION_H
#define POSITION_H

struct Position
{
	int pos_i;
	int pos_j;
	Position(int aPos_i, int aPos_j) : pos_i(aPos_i), pos_j(aPos_j) {}
	Position()
	{
		pos_i = 0;
		pos_j = 0;
	}
};

#endif