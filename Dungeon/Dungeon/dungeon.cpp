#include "position.h"
#include "room.h"
#include "dungeon.h"
#include <vector>
#include <iostream>
#include <string>
#include <stdio.h>
#include <random>
#include <cmath>


Dungeon::Dungeon(int width, int height, int seed)
{
	generator.seed(seed);
	std::srand(seed);

	this->height = height;
	this->width = width;
	this->room_count = generateRandomNumber(1, 20);
	this->default_room = Room();
	this->room_width_dist = std::uniform_int_distribution<int>(2, max_room_width);
	this->room_height_dist = std::uniform_int_distribution<int>(2, max_room_height);
	this->position_i_dist = std::uniform_int_distribution<int>(max_room_height, height - max_room_height);
	this->position_j_dist = std::uniform_int_distribution<int>(max_room_width, width - max_room_width);

	for (int i = 0; i < height; i++)
	{
		this->dungeon.push_back(std::vector<int>(width));
	}
}

void Dungeon::printDungeon()
{
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			switch (dungeon[i][j]){
			case id_tile_solid:
				std::cout << tile_solid << " ";
				break;
			case id_tile_floor:
				std::cout << tile_floor << " ";
				break;
			case id_tile_entrance:
				std::cout << tile_entrance << " ";
				break;
			case id_tile_corridor:
				std::cout << tile_corridor << " ";
				break;
			case id_tile_horizontal_wall:
				std::cout << tile_horizontal_wall << " ";
				break;
			case id_tile_vertical_wall:
				std::cout << tile_vertical_wall << " ";
				break;
			}
		}
		std::cout << std::endl;
	}
}

Room Dungeon::createRoom(Position position)
{
	int room_area = NULL;
	int room_height = NULL;
	int room_width = NULL;
	do
	{
		room_width = room_width_dist(generator);
		room_height = room_height_dist(generator);
		room_area = room_height * room_width;
	} while (room_area > max_room_area && !roomWithinBoundary(position, room_width, room_height));
	Room room = Room(position, room_width, room_height);
	return room;
}

bool Dungeon::roomWithinBoundary(Position room_pos, int room_width, int room_height)
{
	if ((room_pos.pos_i + room_height > height) || (room_pos.pos_j + room_width > width))
	{
		return false;
	}
	return true;
}

void Dungeon::addFirstRoom()
{

	int dungeon_center_i = height / 2;
	int dungeon_center_j = width / 2;
	Position room_position(dungeon_center_i, dungeon_center_j);
	room_position = Position(dungeon_center_i, dungeon_center_j);
	Room room = createRoom(room_position);
	for (int i = room_position.pos_i; i < room_position.pos_i + room.getHeight(); i++)
	{
		for (int j = room_position.pos_j; j < room_position.pos_j + room.getWidth(); j++)
		{
			dungeon[i][j] = id_tile_floor;
			room.fillArea(Position(i, j));
		}
	}
	Position doorPosition = randomRoomWall(room);
	dungeon[doorPosition.pos_i][doorPosition.pos_j] = id_tile_door;
	room.setDoor(doorPosition);
	rooms.push_back(room);
}

bool Dungeon::addRoom()
{
	Position room_position = randomPosition();
	Room room = createRoom(room_position);
	if (!roomsCollide(room))
	{
		if (hasSpaceForRoom(room))
		{
			for (int j = room.getPosition().pos_j; j < room.getPosition().pos_j + room.getWidth(); j++)
			{
				for (int i = room.getPosition().pos_i; i < room.getPosition().pos_i + room.getHeight(); i++)
				{
					dungeon[i][j] = id_tile_floor;
					room.fillArea(Position(i, j));
				}
			}
			Position doorPosition = randomRoomWall(room);
			dungeon[doorPosition.pos_i][doorPosition.pos_j] = id_tile_door;
			room.setDoor(doorPosition);
			rooms.push_back(room);
			return true;
		}
	}
	return false;

}

bool Dungeon::roomsCollide(Room room)
{
	for (size_t i = 0; i < rooms.size(); i++)
	{
		Room anotherRoom = rooms.at(i);
		if (!(anotherRoom == room))
		{
			if ((room.getPosition().pos_i + room.getHeight() < anotherRoom.getPosition().pos_i) ||
				(room.getPosition().pos_i > anotherRoom.getPosition().pos_i + anotherRoom.getHeight()) ||
				(room.getPosition().pos_j + room.getWidth() < anotherRoom.getPosition().pos_j + 1) ||
				(room.getPosition().pos_j > anotherRoom.getPosition().pos_j + anotherRoom.getWidth()))
			{
				return false;
			}
		}
	}
	return true;
}

bool Dungeon::hasSpaceForRoom(Room room)
{
	Position roomPosition = room.getPosition();
	for (int j = roomPosition.pos_j; j < roomPosition.pos_j + room.getWidth(); j++)
	{
		for (int i = roomPosition.pos_i; i < roomPosition.pos_i + room.getHeight(); i++)
		{
			if (dungeon[i][j] != id_tile_solid || i == height - 1 || j == width - 1)
			{
				return false;
			}
		}
	}
	return true;
}

Position Dungeon::randomRoomWall(Room room)
{
	int max_value = room.getArea().size() - 1;
	while (true)
	{
		int random_index = generateRandomNumber(1, max_value);
		Position random_position = room.getArea()[random_index];
		if (random_position.pos_i + 1 < height - 1 && dungeon[random_position.pos_i + 1][random_position.pos_j] == id_tile_solid)
		{
			return Position(random_position.pos_i + 1, random_position.pos_j);
		}
		if (random_position.pos_j - 1 > 0 && dungeon[random_position.pos_i][random_position.pos_j - 1] == id_tile_solid)
		{
			return Position(random_position.pos_i, random_position.pos_j - 1);
		}
		if (random_position.pos_i - 1 > 0 && dungeon[random_position.pos_i - 1][random_position.pos_j] == id_tile_solid)
		{
			return Position(random_position.pos_i - 1, random_position.pos_j);
		}
		if (random_position.pos_j + 1 < width - 1 && dungeon[random_position.pos_i][random_position.pos_j + 1] == id_tile_solid)
		{
			return Position(random_position.pos_i, random_position.pos_j + 1);
		}
	}
}

void Dungeon::generateCorridors()
{
	for (size_t i = 0; i < rooms.size(); i++)
	{
		Room room_1 = rooms.at(i);
		Room room_2 = getClosestRoom(room_1);
		if (room_2 == default_room)
		{
			continue;
		}
		Position position_1 = room_1.getDoor();
		Position position_2 = room_2.getDoor();

		while ((position_2.pos_i != position_1.pos_i) || (position_2.pos_j != position_1.pos_j))
		{
			if (position_2.pos_i != position_1.pos_i)
			{
				if (position_2.pos_i > position_1.pos_i)
				{
					position_2.pos_i -= 1;
				}
				else
				{
					position_2.pos_i += 1;
				}
			}
			else if (position_2.pos_j != position_1.pos_j)
			{
				if (position_2.pos_j > position_1.pos_j)
				{
					position_2.pos_j -= 1;
				}
				else
				{
					position_2.pos_j += 1;
					position_2.pos_i += 1;
				}
			}
			if ((dungeon[position_2.pos_i][position_2.pos_j] == id_tile_solid || dungeon[position_2.pos_i][position_2.pos_j] == id_tile_door) && (position_2.pos_i < height - 1 && position_2.pos_j < width - 1))
			{
				dungeon[position_2.pos_i][position_2.pos_j] = id_tile_corridor;
				corridors.push_back(position_2);
			}
		}
	}
}

Room Dungeon::getClosestRoom(Room room)
{
	Room closest_room = Room();

	int closest_distance = INT_MAX;
	for (size_t i = 0; i < rooms.size(); i++)
	{
		if (!(rooms.at(i) == room))
		{
			Room anotherRoom = rooms.at(i);
			int distance = std::abs(room.getDoor().pos_i - anotherRoom.getDoor().pos_i) + std::abs(room.getDoor().pos_j - anotherRoom.getDoor().pos_j);
			if (distance < closest_distance)
			{
				closest_distance = distance;
				closest_room = anotherRoom;
			}
		}
	}
	return closest_room;
}

void Dungeon::buildDungeon()
{
	addFirstRoom();
	bool roomAdded = false;
	//printDungeon();
	while ((int)rooms.size() < room_count)
	{
		roomAdded = addRoom();
		//printDungeon();
		if (roomAdded && rooms.size() > 1)
		{
			generateCorridors();
			//printDungeon();
		}
	}
	generateCorridors();
	adjustDungeon();
	addEntrance();
	printLegend();
	printDungeon();
}

void Dungeon::addWalls()
{
	for (int i = 1; i < height - 1; i++)
	{
		for (int j = 1; j < width - 1; j++)
		{
			if (dungeon[i][j] == id_tile_floor || dungeon[i][j] == id_tile_corridor)
			{
				if (dungeon[i + 1][j] == id_tile_solid)
					dungeon[i + 1][j] = id_tile_horizontal_wall;
				if (dungeon[i - 1][j] == id_tile_solid)
					dungeon[i - 1][j] = id_tile_horizontal_wall;
				if (dungeon[i][j + 1] == id_tile_solid)
					dungeon[i][j + 1] = id_tile_vertical_wall;
				if (dungeon[i][j - 1] == id_tile_solid)
					dungeon[i][j - 1] = id_tile_vertical_wall;
			}
		}
	}
}

/* This function adds floors to rooms and corridors and closes some open spaces with walls.*/
void Dungeon::lastAdjustments()
{
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (dungeon[i][j] == id_tile_floor)
			{
				// id_tile corridor will be shown eventually as the floor of the dungeon (which includes rooms also)
				dungeon[i][j] = id_tile_corridor;
			}
			if (dungeon[i][j] == id_tile_vertical_wall)
			{
				if (i + 1 < height)
				{
					if (dungeon[i + 1][j] == id_tile_solid)
					{
						dungeon[i + 1][j] = id_tile_horizontal_wall;
					}
				}
				if (i - 1 > 0)
				{
					if (dungeon[i - 1][j] == id_tile_solid)
					{
						dungeon[i - 1][j] = id_tile_horizontal_wall;
					}
				}
			}
			if (dungeon[i][j] == id_tile_horizontal_wall)
			{
				if (i - 1 > 0 && j - 1 > 0)
				{
					if (dungeon[i - 1][j] == id_tile_corridor && dungeon[i][j - 1] == id_tile_solid)
					{
						dungeon[i][j - 1] = id_tile_vertical_wall;
					}
				}
			}
		}
	}
}

void Dungeon::adjustDungeon()
{
	addWalls();
	lastAdjustments();
}

void Dungeon::addEntrance()
{
	for (int i = height - 1; i >= 0; i--)
	{
		for (int j = width - 1; j >= 0; j--)
		{
			if (dungeon[i][j] == id_tile_horizontal_wall && i - 1 > 0)
			{
				if (dungeon[i - 1][j] != id_tile_vertical_wall)
				{
					dungeon[i][j] = id_tile_entrance;
					return;
				}
			}
		}
	}
}

Position Dungeon::randomPosition()
{
	int random_i = position_i_dist(generator);
	int random_j = position_j_dist(generator);

	return Position(random_i, random_j);
}

int Dungeon::generateRandomNumber(int start_range, int end_range)
{
	return start_range + (std::rand() % (end_range - start_range + 1));
}

void Dungeon::printLegend()
{
	std::cout << tile_horizontal_wall << ": " << "Horizontal Wall" << std::endl;
	std::cout << tile_vertical_wall << ": " << "Vertical Wall" << std::endl;
	std::cout << tile_corridor << ": " << "Floor - includes corridors and rooms" << std::endl;
	std::cout << tile_entrance << ": " << "Entrance/Exit" << std::endl;
	std::cout << std::endl;
}